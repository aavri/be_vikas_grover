package com.aavri.commons.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class LegalEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    private String externalId;
    private String name;
    private String parentExternalId;
    private String type;

    public LegalEntity() {
        super();
    }

    public LegalEntity(String externalId, String name, String parentExternalId, String type) {
        this.externalId = externalId;
        this.name = name;
        this.parentExternalId = parentExternalId;
        this.type = type;
    }

    @Override
    public String toString() {
        return "LegalEntity{" +
                "externalId='" + externalId + '\'' +
                ", name='" + name + '\'' +
                ", parentExternalId='" + parentExternalId + '\'' +
                ", type='" + type + '\'' +
                '}';
    }
}
