package com.aavri.commons.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class Arrangement implements Serializable {

    private static final long serialVersionUID = 1L;
    private String id;
    private String name;
    private String productId;
    private String legalEntityId;
    private String alias;
    private Double bookedBalance;
    private Double availableBalance;
    private Double creditLimit;
    private String IBAN;
    private String BBAN;
    private String currency;
    private Boolean externalTransferAllowed;
    private Boolean urgentTransferAllowed;
    private Double accruedInterest;
    private String number;
    private Double principalAmount;
    private Double currentInvestmentValue;
    private String productNumber;
    private String BIC;
    private String bankBranchCode;
    private String accountOpeningDate;
    private Double accountInterestRate;
    private Double valueDateBalance;
    private Double creditLimitUsage;
    private Double creditLimitInterestRate;
    private Double creditLimitExpiryDate;
    private String startDate;
    private String termUnit;
    private Integer termNumber;
    private String maturityDate;
    private Double maturityAmount;
    private Boolean autoRenewalIndicator;
    private String interestPaymentFrequencyUnit;
    private Integer interestPaymentFrequencyNumber;
    private String interestSettlementAccount;
    private Double outstandingPrincipalAmount;
    private Double monthlyInstalmentAmount;
    private Double amountInArrear;
    private Double minimumRequiredBalance;
    private String creditCardAccountNumber;
    private String validThru;
    private Double applicableInterestRate;
    private Double remainingCredit;
    private Double outstandingPayment;
    private Double minimumPayment;
    private String minimumPaymentDueDate;
    private Double totalInvestmentValue;
    private List<DebitCard> debitCards = new ArrayList<>(1);
    private String accountHolderAddressLine1;
    private String accountHolderAddressLine2;
    private String accountHolderStreetName;
    private String town;
    private String postCode;
    private String countrySubDivision;
    private String accountHolderName;
    private String accountHolderCountry;
    private Boolean creditAccount;
    private Boolean debitAccount;
    private String lastUpdateDate;

    public Arrangement() {
        super();
    }

    @Override
    public String toString() {
        return "Arrangement{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", productId='" + productId + '\'' +
                ", legalEntityId='" + legalEntityId + '\'' +
                '}';
    }
}

@Getter
@Setter
class DebitCard implements Serializable {
    private static final long serialVersionUID = 1L;
    private String number;
    private String expiryDate;

    public DebitCard() {
        super();
    }
}
