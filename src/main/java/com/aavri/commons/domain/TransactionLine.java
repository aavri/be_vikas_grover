package com.aavri.commons.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class TransactionLine implements Serializable {

    private static final long serialVersionUID = 1L;
    private String id;
    private String arrangementId;
    private String externalId;
    private String externalArrangementId;
    private String reference;
    private String description;
    private String typeGroup;
    private String type;
    private String category;
    private String bookingDate;
    private String valueDate;
    private String creditDebitIndicator;
    private Currency transactionAmountCurrency;
    private Currency instructedAmountCurrency;
    private Double amount;
    private String currency;
    private Double instructedAmount;
    private String instructedCurrency;
    private Double currencyExchangeRate;
    private String counterPartyName;
    private String counterPartyAccountNumber;
    private Integer checkSerialNumber;

    public TransactionLine() {
        super();
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "id='" + id + '\'' +
                ", arrangementId='" + arrangementId + '\'' +
                ", externalId='" + externalId + '\'' +
                ", externalArrangementId='" + externalArrangementId + '\'' +
                ", reference='" + reference + '\'' +
                '}';
    }

}
