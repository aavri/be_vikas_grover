package com.aavri.commons.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductResponse extends Product implements Serializable {

    private static final long serialVersionUID = 1L;

    private String internalId;

    public ProductResponse() {
        super();
    }

    @Override
    public String toString() {
        return "ProductResponse{" +
                "internalId='" + internalId + '\'' +
                '}';
    }
}
