package com.aavri.commons.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class Product implements Serializable {

    private static final long serialVersionUID = 1L;
    private String id;
    private String productKindId;
    private String productKindName;
    private String productTypeId;
    private String productTypeName;

    public Product() {
        super();
    }

    @Override
    public String toString() {
        return "Product{" +
                "id='" + id + '\'' +
                ", productKindId='" + productKindId + '\'' +
                ", productKindName='" + productKindName + '\'' +
                ", productTypeId='" + productTypeId + '\'' +
                ", productTypeName='" + productTypeName + '\'' +
                '}';
    }
}
