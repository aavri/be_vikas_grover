package com.aavri.commons.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Currency implements Serializable {

    private static final long serialVersionUID = 1L;

    private String amount;
    private String currencyCode;

    @Override
    public String toString() {
        return "Currency{" +
                "amount='" + amount + '\'' +
                ", currencyCode='" + currencyCode + '\'' +
                '}';
    }
}
