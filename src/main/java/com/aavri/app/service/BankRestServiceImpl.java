package com.aavri.app.service;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

import com.aavri.commons.domain.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.*;

import com.aavri.app.controller.exception.BankRestClientException;
import org.springframework.core.ParameterizedTypeReference;

/**
 * <h1>Rest Calls to Backbase API</h1>
 *
 * @author  Vikas
 * @version 1.0
 */
@Service
public class BankRestServiceImpl implements BankRestService {

    private static final Logger logger = LoggerFactory.getLogger(BankRestServiceImpl.class);

    @Value("${backbase.integration.url:http://localhost:8086/}")
    private String integrationBaseUrl;

    @Value("${backbase.products.summary.url:http://localhost:8082/}")
    private String productBaseUrl;

    @Value("${backbase.transactions.url:http://localhost:8083/}")
    private String transactionsBaseUrl;

    @Value("${legalEntities.api.url:legalentity-integration-servica/v2/legalentities}")
    private String legalEntitiesApiUrl;

    @Value("${products.api.url:arrangements-integration-service/v2/products}")
    private String productsApiUrl;

    @Value("${arrangements.api.url:arrangements-integration-service/v2/arrangements}")
    private String arrangementsApiUrl;

    @Value("${transactions.api.url:transaction-integration-service/v2/transactions}")
    private String transactionsApiUrl;

    @Autowired
    private RestTemplate restTemplate;

    private static final String BACKBASE_INTERNAL_SERVICE_ERROR = "Error processing request. Please check if Backbase services are running.";
    private static final String BAD_REQUEST = "Error processing request. Please check if json files are placed in correct folder and has no errors";
    private static final String OTHER_RUNTIME_EXCEPTION = "Error processing the request. Please check if all Backbase services are running.";
    private static final String OTHER_EXCEPTION = "Exception occurred. Please check if the environment is setup correctly";

    public void postLegalEntities(Set<LegalEntity> entities) {
        HttpEntity<LegalEntity> request;
        for (LegalEntity legalEntity : entities) {
            try {
                request = new HttpEntity<>(legalEntity, setHeaders());
                restTemplate.postForObject(integrationBaseUrl + legalEntitiesApiUrl, request, LegalEntity.class);
                logger.info("Legal Entity: " + legalEntity.getExternalId());
            } catch (HttpStatusCodeException ex) {
                processResponseExceptions(ex, "postLegalEntities");
            } catch (Exception ex) {   // ByPassing Type Definition Error
                logger.error("Legal Entity Generic Exception " + ex.getMessage());
            }
        }
    }

    public List<ProductResponse> getProducts() {
        HttpEntity<List<ProductResponse>> products = new HttpEntity<>(setHeaders());
        try {
            return restTemplate.exchange(productBaseUrl + productsApiUrl, HttpMethod.GET, products, new ParameterizedTypeReference<List<ProductResponse>>() {
            }).getBody();
        } catch (HttpStatusCodeException ex) {
            processResponseExceptions(ex, "getProducts");
        } catch (Exception ex) {
            logger.error("Generic Exception " + ex.getMessage());
        }
        return null;
    }

    public void postProducts(List<Product> products) {
        HttpEntity<Product> request;
        for (Product product : products) {
            try {
                request = new HttpEntity<>(product, setHeaders());
                restTemplate.postForObject(productBaseUrl + productsApiUrl, request, Product.class);
                logger.info("Product: " + product.getId());
            } catch (HttpStatusCodeException ex) {
                processResponseExceptions(ex, "postProducts");
            } catch (Exception ex) {   // ByPassing Type Definition Error
                logger.error("Post Products Generic Exception " + ex.getMessage());
            }
        }
    }

    public void postArrangements(List<Arrangement> arrangements) {
        HttpEntity<Arrangement> request;
        for (Arrangement arrangement : arrangements) {
            try {
                request = new HttpEntity<>(arrangement, setHeaders());
                restTemplate.postForObject(productBaseUrl + arrangementsApiUrl, request, Arrangement.class);
                logger.info("Arrangement: " + arrangement.getName());
            } catch (HttpClientErrorException ex) {
                logger.error("Arrangement Client Exception " + ex.getStatusCode() + " - " + ex.getResponseBodyAsString());
                if (!isAlreadyExistsException(ex)) {
                    throw new BankRestClientException("Arrangement Service Error " + ex.getMessage());
                }
            } catch (Exception ex) {
                logger.error("Arrangements Generic Exception " + ex.getLocalizedMessage());
            }
        }
    }

    public void postTransactions(List<TransactionLine> transactions) {
        HttpEntity<List<TransactionLine>> request = new HttpEntity<>(transactions, setHeaders());
        try {
            restTemplate.postForEntity(transactionsBaseUrl + transactionsApiUrl, request, List.class);
            logger.info("Transaction Posted ");
        } catch (HttpStatusCodeException ex) {
            logger.info(request.getBody().toString());
            processResponseExceptions(ex, "postTransactions");
        } catch (Exception ex) {
            logger.error("Transactions Generic Exception " + ex.getMessage());
        }
    }

    private HttpHeaders setHeaders() {
        // HttpHeaders
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(new MediaType[]{MediaType.APPLICATION_JSON}));
        // Request to return JSON format
        headers.setContentType(MediaType.APPLICATION_JSON);
        return headers;
    }

    private Boolean isAlreadyExistsException(HttpStatusCodeException ex) {
        if (HttpStatus.BAD_REQUEST.equals(ex.getStatusCode())) {
            if (ex.getResponseBodyAsString().contains("already exists")) {
                return true;
            }
        }
        return false;
    }

    private Boolean isInternalServerError(HttpServerErrorException ex) {
        if (HttpStatus.INTERNAL_SERVER_ERROR.equals(ex.getStatusCode())) {
            return true;
        }
        return false;
    }

    private void processResponseExceptions(HttpStatusCodeException ex, String methodName) {
        logger.info(String.format("Exception in %s: %s - %s", methodName, ex.getStatusCode(), ex.getResponseBodyAsString()));
        if (HttpStatus.INTERNAL_SERVER_ERROR.equals(ex.getStatusCode()) || (HttpStatus.BAD_GATEWAY.equals(ex.getStatusCode()))) {
            throw new BankRestClientException(BACKBASE_INTERNAL_SERVICE_ERROR);
        } else if (HttpStatus.BAD_REQUEST.equals(ex.getStatusCode())) {
            if (!isAlreadyExistsException(ex)) {
                throw new BankRestClientException(BAD_REQUEST);
            }
        } else {
            throw new BankRestClientException(OTHER_RUNTIME_EXCEPTION);
        }
    }

}
