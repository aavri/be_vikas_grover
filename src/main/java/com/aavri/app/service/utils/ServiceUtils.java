package com.aavri.app.service.utils;

import java.util.*;

import com.aavri.commons.domain.Arrangement;
import com.aavri.commons.domain.LegalEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class ServiceUtils {
    private final static Logger logger = LoggerFactory.getLogger(ServiceUtils.class);

    private static final String BANK_ROOT = "Bank";
    private static final String BANK = "BANK";
    private static final String CUSTOMER = "CUSTOMER";

    private ServiceUtils() {
        super();
    }


	public static Set<LegalEntity> extractLegalEntities(List<Arrangement> arrangements ) {
        Set<LegalEntity> entities = new LinkedHashSet<>();
        entities.add(new LegalEntity(BANK_ROOT, BANK, null, BANK));
        for (Arrangement arrangement: arrangements) {
            entities.add(new LegalEntity(arrangement.getLegalEntityId(), arrangement.getLegalEntityId(), BANK_ROOT, CUSTOMER));
		}
        for (LegalEntity legalEntity : entities) {
            logger.debug(" Legal Entity " + legalEntity);
        }
		return entities;
	}
	

}
