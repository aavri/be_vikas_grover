package com.aavri.app.service;

import java.util.List;
import java.util.Set;

import com.aavri.commons.domain.Arrangement;
import com.aavri.commons.domain.LegalEntity;
import com.aavri.commons.domain.Product;
import com.aavri.commons.domain.ProductResponse;
import com.aavri.commons.domain.TransactionLine;

public interface BankService {

	List<ProductResponse> getProducts();

	void postLegalEntities(Set<LegalEntity> entities);

	void postProducts(List<Product> products);

	void postArrangements(List<Arrangement> arrangements);

	void postTransactions(List<TransactionLine> transactions);

}
