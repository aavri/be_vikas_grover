package com.aavri.app.service;

import java.util.List;
import java.util.Set;

import com.aavri.commons.domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
* <h1>Implementation of Banking Service</h1>
* Calls Banking API using Backbase Rest API, parse data

* @author  Vikas
* @version 1.0
*/
@Service
public class BankServiceImpl implements BankService {
	
	@Autowired
	private BankRestService bankRestServices;

	/**
	* Get All Products
	* Calls Banking API and retrieves all saved products
	*
	* @return List of Products
	*/
	@Override
	public List<ProductResponse> getProducts() {
		return bankRestServices.getProducts();
	}

	@Override
	public void postLegalEntities(Set<LegalEntity> entities) {
		bankRestServices.postLegalEntities(entities);
	}

	@Override
	public void postProducts(List<Product> products) {
		bankRestServices.postProducts(products);
	}

	@Override
	public void postArrangements(List<Arrangement> arrangements) {
		bankRestServices.postArrangements(arrangements);
	}

	@Override
	public void postTransactions(List<TransactionLine> transactions) {
		bankRestServices.postTransactions(transactions);
	}
	
}
