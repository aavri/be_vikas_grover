package com.aavri.app.controller.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR, reason = "Rest API Unreachable")
public class BankRestClientException extends RuntimeException {
	private static final long serialVersionUID = 100L;

	public BankRestClientException(String message) {
		super(message);
	}
}
