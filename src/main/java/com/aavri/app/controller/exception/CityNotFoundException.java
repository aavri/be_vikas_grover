package com.aavri.app.controller.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "No branch exist for requested city.")
public class CityNotFoundException extends RuntimeException {
	private static final long serialVersionUID = 100L;

	public CityNotFoundException(String message) {
		super(message);
	}
}
