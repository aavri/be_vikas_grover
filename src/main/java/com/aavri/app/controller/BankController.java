package com.aavri.app.controller;

import java.util.List;
import com.aavri.app.service.utils.ServiceUtils;
import com.aavri.commons.domain.*;
import com.aavri.mapperapp.reader.JSONReaderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.aavri.app.service.BankService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * <h1>Main Controller to Post Mock Data</h1>
 *
 * @author Vikas
 * @version 1.0
 */
@RestController
@RequestMapping("/api/v1/data")
public class BankController {

    private final static Logger logger = LoggerFactory.getLogger(BankController.class);

    @Autowired
    private BankService bankService;

    @Autowired
    private JSONReaderService readerService;

    @ApiOperation(value = "get a list of all products", response = Product.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list"),
            @ApiResponse(code = 404, message = "Resource Not Found"),
            @ApiResponse(code = 500, message = "Internal Server Error")})
    @GetMapping
    public List<ProductResponse> getProducts() {
        return bankService.getProducts();
    }

    @ApiOperation(value = "Endpoint to post mock data")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list"),
            @ApiResponse(code = 404, message = "Resource Not Found"),
            @ApiResponse(code = 500, message = "Internal Server Error")})
    @PostMapping
    public void postData() {
        try {
            List<Product> products = readerService.parseProducts();
            bankService.postProducts(products);

            List<Arrangement> arrangements = readerService.parseArrangements();
            bankService.postLegalEntities(ServiceUtils.extractLegalEntities(arrangements));
            bankService.postArrangements(arrangements);

            List<TransactionLine> transactions = readerService.parseTransactions();
            bankService.postTransactions(transactions);
            logger.info("**** Mock Data Posted, You can end this process now by pressing Ctrl+C ****");
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            throw ex;
        }
    }

}
