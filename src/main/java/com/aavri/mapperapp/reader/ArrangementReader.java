package com.aavri.mapperapp.reader;

import com.aavri.commons.domain.Arrangement;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.List;

public final class ArrangementReader {

    private final static Logger logger = LoggerFactory.getLogger(ArrangementReader.class);

    private static ObjectMapper objectMapper = new ObjectMapper();

    private ArrangementReader() {
        super();
    }

    public static List<Arrangement> readParseList(String fileName) throws IOException {
        JavaType javaType = objectMapper.getTypeFactory().constructParametricType(List.class, Arrangement.class);
        return objectMapper.readValue(new File(fileName), javaType);
    }

    public static void print(Collection<Arrangement> arrangements) {
        for (Arrangement arrangement : arrangements) {
            logger.info(arrangement.toString());
        }
    }
}
