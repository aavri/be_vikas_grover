package com.aavri.mapperapp.reader;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.aavri.commons.domain.Product;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Collection;

import com.fasterxml.jackson.core.type.TypeReference;

import com.fasterxml.jackson.databind.JavaType;

public final class ProductReader {

    private final static Logger logger = LoggerFactory.getLogger(ProductReader.class);

    private static ObjectMapper objectMapper = new ObjectMapper();

    private ProductReader() {
        super();
    }

    public static Product[] readParseArray(String fileName) throws IOException {
        return objectMapper.readValue(new File(fileName), Product[].class);
    }

    public static Collection<Product> readParseCollection(String fileName) throws IOException {
        return objectMapper.readValue(
                new File(fileName), new TypeReference<Collection<Product>>() {
                }
        );
    }

    public static List<Product> readParseList(String fileName) throws IOException {
        JavaType javaType = objectMapper.getTypeFactory().constructParametricType(List.class, Product.class);
        return objectMapper.readValue(new File(fileName), javaType);
    }

    public static void print(Collection<Product> products) {
        for (Product p : products) {
            logger.info(p.toString());
        }
    }

    public static void print(Product[] products) {
        for (int i = 0; i < products.length; i++) {
            logger.info(products[i].toString());
        }
    }
}
