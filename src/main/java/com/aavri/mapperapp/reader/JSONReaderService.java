package com.aavri.mapperapp.reader;

import com.aavri.app.controller.exception.BankRestClientException;
import com.aavri.commons.domain.Arrangement;
import com.aavri.commons.domain.Product;
import com.aavri.commons.domain.TransactionLine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

/**
 * <h1>Main Mapper Service</h1>
 * Calls Appropriate Reader to parse json file into objects
 *
 * @author Vikas
 * @version 1.0
 */
@Service
public class JSONReaderService {
    private final static Logger logger = LoggerFactory.getLogger(JSONReaderService.class);

    @Value("${json.config.folder}")
    private String configFolder;

    @Value("${products.json.filename}")
    private String productsFile;

    @Value("${arrangements.json.filename}")
    private String arrangementsFile;

    @Value("${transactions.json.filename}")
    private String transactionsFile;

    private JSONReaderService() {
        super();
    }

    public List<Product> parseProducts() {
        logger.info("Begin Reading Products");
        try {
            return ProductReader.readParseList(configFolder + productsFile);
        } catch (IOException ex) {
            throw new BankRestClientException("Error Reading " + ex.getLocalizedMessage());
        }
    }

    public List<Arrangement> parseArrangements() {
        logger.info("Begin Reading Arrangements");
        try {
            return ArrangementReader.readParseList(configFolder + arrangementsFile);
        } catch (IOException ex) {
            throw new BankRestClientException("Error Reading " + ex.getLocalizedMessage());
        }
    }

    public List<TransactionLine> parseTransactions() {
        logger.info("Begin Reading Transactions");
        try {
            return TransactionReader.readParseJSON(configFolder + transactionsFile);
        } catch (IOException ex) {
            throw new BankRestClientException("Error Reading " + ex.getLocalizedMessage());
        }
    }

    public static void main(String[] args) {
        JSONReaderService reader = new JSONReaderService();
        reader.parseProducts();
        reader.parseArrangements();
        reader.parseTransactions();
    }

}
