package com.aavri.mapperapp.reader;

import com.aavri.commons.domain.TransactionLine;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.List;

public final class TransactionReader {

    private final static Logger logger = LoggerFactory.getLogger(TransactionReader.class);

    private static ObjectMapper objectMapper = new ObjectMapper();

    private TransactionReader() {
        super();
    }

    public static List<TransactionLine> readParseList(String fileName) throws IOException {
        JavaType javaType = objectMapper.getTypeFactory().constructParametricType(List.class, TransactionLine.class);
        return objectMapper.readValue(new File(fileName), javaType);
    }

    public static List<TransactionLine> readParseJSON(String fileName) throws IOException {
        return objectMapper.reader().forType(new TypeReference<List<TransactionLine>>() {}).readValue(new File(fileName));
    }


    public static void print(Collection<TransactionLine> transactions) {
        for (TransactionLine transaction : transactions) {
            logger.info(transaction.toString());
        }
    }
}
