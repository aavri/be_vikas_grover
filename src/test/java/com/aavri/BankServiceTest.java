package com.aavri;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.aavri.app.service.BankService;
import com.aavri.app.service.BankServiceImpl;
import com.aavri.app.service.BankRestService;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
public class BankServiceTest { 
/*
	@InjectMocks
	private BankService mockBankService = new BankServiceImpl();
	
	@Mock
	private BankRestService mockBankRestService;
	
	private BranchResult branchResult;
    
	@Before
	public void setUp() {

		Datum datum = new Datum();
		
		List<Branch> branches = new ArrayList<>(1);
		Branch branch = new Branch();
		branch.setName("branch1");
		branch.setPostalAddress(createAddress("address1", "41.1", "-21.1", "PC0001", "LIVERPOOL"));
		branches.add(branch);
		
		branch = new Branch();
		branch.setName("branch2");
		branch.setPostalAddress(createAddress("address2", "42.1", "-22.1", "PC0002", "LIVERPOOL"));
		branches.add(branch);
		
		branch = new Branch();
		branch.setName("branch3");
		branch.setPostalAddress(createAddress("address3", "43.1", "-23.1", "PC0003", "LONDON"));
		branches.add(branch);
		
		branch = new Branch();
		branch.setName("branch4");
		branch.setPostalAddress(createAddress("address4", "44.1", "-24.1", "PC0004", "LIVERPOOL"));
		branches.add(branch);
		
		List<Brand> brands = new ArrayList<>(1);
		Brand brand = new Brand();
		brand.setBranch(branches);
		brands.add(brand);
		datum.setBrand(brands);
		
		List<Datum> datums = new ArrayList<>(1);
		datums.add(datum);
		branchResult = new BranchResult();
		branchResult.setData(datums);
		
		Mockito.when(mockBankRestService.getBranchResult()).thenReturn(branchResult);
	}
	
	@Test
	public void getAllBranches() {
		List<BranchDTO> branchDtos = mockBankService.getAllBranches();
		assertEquals(4, branchDtos.size());
	}
	
	@Test
	public void getCityBranches() {
		List<BranchDTO> branchDtos = mockBankService.getCityBranches("LONDON");
		assertEquals(1, branchDtos.size());
		branchDtos = mockBankService.getCityBranches("LIVERPOOL");
		assertEquals(3, branchDtos.size());		
	}	
	
	private static PostalAddress createAddress(String address, String latitude, String longitude, String postalCode, String townName) {
		GeoLocation geoLocation = new GeoLocation();
		GeographicCoordinates coord = new GeographicCoordinates();
		coord.setLatitude(latitude);
		coord.setLongitude(longitude);
		geoLocation.setGeographicCoordinates(coord);
		
		PostalAddress postalAddress = new PostalAddress();		
		postalAddress.setAddressLine(new ArrayList<String>(1) {{add(address);}});
		postalAddress.setGeoLocation(geoLocation);
		postalAddress.setPostCode(postalCode);
		postalAddress.setTownName(townName);
		return postalAddress;
	}
*/
}
