**Project Title**

Banking Service Mock API

**Getting Started**

These instructions will install and run Mock API on local machine. See deployment for notes on how to deploy the project on a live system.

**Prerequisites**

`JDK 1.8+ 
maven 3.5+`

**Default Port**

`8680`

**How to run**

Download the zip file
Unzip the zip file to a folder of you choice (project folder)

Open the terminal window and navigate to project's root folder and run command

`mvn spring-boot:run`

**Mocking Data**

_**Step#1 :**_ 

Create 3 text files products.json, arrangements.json and transactions.json from the sample json data 
and save all these files in your computer's _**/tmp**_ folder 

All 3 sample files are available at project root 

_**Step#2 :**_ 

Using Postman or any other REST Client, execute following POST request

`POST http://localhost:8680/api/v1/data`

Set Header's `Content-Type: application/json`

Press Send Button to post mock data, if json data is correct, product, arrangements and Transactions will be persisted to Backbase

API Documentation:
http://localhost:8680/swagger-ui.html

Authors

Vikas 

